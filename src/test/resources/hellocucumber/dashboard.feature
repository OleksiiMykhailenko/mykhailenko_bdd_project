@dashboard-page

Feature: Check Dash Board Page

  Scenario: Check 'What's new' page in Dashboard
    Given user clicks whats new page button
    Then elements on page are present

  Scenario: Goes to the 'My Payments' page and check categories
    Given user opens my payments menu
    When check that templates block is present
    Then check categories in category bloc

  Scenario Outline: Check 'Send Money' page in Public session
    Given open send money menu
    When user filling "<sendersCard>", "<expireDate>" and "<cvvCode>" in all fields of the sender's card
    And user filling in all fields of the receiver "<receiverCard>"
    And user filling out the amount field
    And user chooses the "<currency>"
    And user add comment
    And user clicks send button
    Then check sending "<message>" money in public session is not allowed
    Examples:
      | sendersCard      | expireDate | cvvCode | receiverCard     | currency | message                                                                   |
      | 4149437866860966 | 1125       | 777     | 4441114422073484 | USD      | Тимчасово перекази з картки можна здійснювати лише після входу в Приват24 |

  Scenario Outline: Uses the search menu and goes to the page we found
    Given user clicks on find button and search the "<query>" in the header of the website
    Then check elements and "<title>" are present on Instant Installment Page
    Examples:
      | query   | title                         |
      | Миттєва | Купуйте зараз – платіть потім |

  Scenario Outline: Check currency converter in Dashboard page
    Given choose "<currency1>" and "<currency2>" in Currency Converter
    Then set currency value and check rate
    Examples:
      | currency1 | currency2 |
      | EUR       | UAH       |

  Scenario: Check elements on login frame use actions
    Given check that login frame is present
    Then check elements on Login frame