package hellocucumber;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import static utils.Constants.*;

/**
 * Dash Board step Class. Here are tests for working with the site's dashboard
 */
public class StepDefinitions extends BaseStepDefinitions {

    WebDriver driver;

    public StepDefinitions() {
        super.init();
        super.getBaseUrl();
        PageFactory.initElements(driver, this);
    }

    @Given("user clicks whats new page button")
    public void userClicksWhatsNewPageButton() {
        dashBoardPage.openWhatsNewPage();
    }

    @Then("elements on page are present")
    public void elementsOnPageArePresent() {
        dashBoardPage.findElementsOnWhatsNewPageAndCheck();
    }


    @Given("user opens my payments menu")
    public void userOpensMyPaymentsMenu() {
        dashBoardPage.goToPaymentsMenu();
    }

    @When("check that templates block is present")
    public void checkThatTemplatesBlockIsPresent() {
        dashBoardPage.popularTemplatesBlockPublicSessionIsPresent();
    }

    @Then("check categories in category bloc")
    public void checkCategoriesInCategoryBloc() {
        dashBoardPage.checkCategoriesList(categoriesList);
    }


    @Given("open send money menu")
    public void openSendMoneyMenu() {
        dashBoardPage.goToMenuSendMoneyOnPublicSessionAndCheck();
    }

    @When("user filling {string}, {string} and {string} in all fields of the sender's card")
    public void userFillingAndInAllFieldsOfTheSenderSCard(String arg0, String arg1, String arg2) {
        basePage.fillFieldsSendersCard(arg0, arg1, arg2);
    }

    @And("user filling in all fields of the receiver {string}")
    public void userFillingInAllFieldsOfTheReceiver(String arg0) {
        basePage.fillFieldsReceiverCard(arg0);
    }

    @And("user filling out the amount field")
    public void userFillingOutTheAmountField() {
        basePage.fillAmount("1");
    }

    @And("user chooses the {string}")
    public void userChoosesThe(String arg0) {
        basePage.chooseCurrency(arg0);
    }

    @And("user add comment")
    public void userAddComment() {
        basePage.addComment("test");
    }

    @And("user clicks send button")
    public void userClicksSendButton() {
        basePage.clickSendButton();
    }

    @Then("check sending {string} money in public session is not allowed")
    public void checkSendingMoneyInPublicSessionIsNotAllowed(String arg0) {
        basePage.checkMessage(arg0);
    }


    @Given("user clicks on find button and search the {string} in the header of the website")
    public void userClicksOnFindButtonAndSearchTheInTheHeaderOfTheWebsite(String arg0) {
        dashBoardPage.searchPage(arg0);
    }

    @Then("check elements and {string} are present on Instant Installment Page")
    public void checkElementsAndArePresentOnInstantInstallmentPage(String arg0) {
        dashBoardPage.checkElementsOnInstantInstallmentPage(arg0);
    }


    @Given("choose {string} and {string} in Currency Converter")
    public void chooseAndInCurrencyConverter(String arg0, String arg1) {
        dashBoardPage.chooseCurrency(arg0, arg1);
    }

    @Then("set currency value and check rate")
    public void setCurrencyValueAndCheckRate() {
        dashBoardPage.setCurrencyValueAndCheckRate("11");
    }


    @Given("check that login frame is present")
    public void checkThatLoginFrameIsPresent() {
        dashBoardPage.moveToLoginFrame();
    }

    @Then("check elements on Login frame")
    public void checkElementsOnLoginFrame() {
        dashBoardPage.checkElementOnLoginFrame();
    }


    @Override
    public void tearDown() {
        super.tearDown();
    }
}
