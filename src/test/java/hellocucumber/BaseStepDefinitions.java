package hellocucumber;

import io.cucumber.java.After;
import org.junit.jupiter.api.DisplayName;
import org.openqa.selenium.WebDriver;
import pages.BasePage;
import pages.DashBoardPage;
import utils.PropertiesFactory;
import utils.WebDriverFactory;

import static utils.Constants.PATH_TO_PROPERTY_FILE;
import static utils.WebDriverFactory.createDriver;

/**
 * A base step class to initialize our driver and use methods before each test
 */
public class BaseStepDefinitions {

    private static WebDriver driver;
    public static BasePage basePage;
    public static DashBoardPage dashBoardPage;

    @DisplayName("initialize our driver")
    public void init() {
        PropertiesFactory propertiesFactory = new PropertiesFactory(PATH_TO_PROPERTY_FILE);
        driver = createDriver(propertiesFactory.getBrowserProperty());
        dashBoardPage = new DashBoardPage(driver);
        basePage = new BasePage(driver);
    }

    @DisplayName("get base url to use it in tests")
    public void getBaseUrl() {
        PropertiesFactory propertiesFactory = new PropertiesFactory(PATH_TO_PROPERTY_FILE);
        driver.get(propertiesFactory.getProperty("base_url"));
    }

    @After
    @DisplayName("Close Driver after each test")
    public void tearDown() {
        if (driver != null) {
            WebDriverFactory.closeDriver();
        }
    }
}
