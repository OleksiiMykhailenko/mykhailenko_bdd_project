package utils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

/**
 * Properties class in Selenium allows loading and retrieving values from properties files.
 */
public class PropertiesFactory {

    private Properties properties;

    /**
     * Property factory
     *
     * @param propertyFilePath path to our property file
     */
    public PropertiesFactory(String propertyFilePath) {
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(propertyFilePath));
            properties = new Properties();
            try {
                properties.load(reader);
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Configuration.properties not found at " + propertyFilePath);
        }
    }

    /**
     * Return key from property
     *
     * @param key key word in file
     */
    public String getProperty(String key) {
        String driverPath = properties.getProperty(key);
        if (driverPath != null) {
            return driverPath;
        } else {
            throw new RuntimeException("DriverPath not specified in the Configuration.properties file.");
        }
    }

    /**
     * Return browser from property
     */
    public String getBrowserProperty() {
        String url = properties.getProperty("browser");
        if (url != null) {
            return url;
        } else {
            throw new RuntimeException("Url not specified in the Configuration.properties file.");
        }
    }
}
