package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;

import java.time.Duration;

import static utils.Constants.DEFAULT_TIMEOUT;

/**
 * This is a class with a factory method that is responsible for performing all the complex actions required
 */
public class WebDriverFactory {

    public static WebDriver driver;

    private WebDriverFactory() {
    }

    /**
     * Call this method if WebDriver is not initialized!
     */
    public static WebDriver getDriver() {
        if (driver == null) {
            throw new IllegalStateException("WebDriver instance has not been initialized! Call to method createDriver!");
        }
        return driver;
    }

    /**
     * Fabric Web Driver
     *
     * @param driverName variant of driver, where we run our tests
     */
    public static WebDriver createDriver(final String driverName) {
        if (driver != null) {
            throw new IllegalStateException("WebDriver instance has already been initialized.");
        }

        switch (driverName) {
            case "firefox":
                driver = new FirefoxDriver();
                break;
            case "chrome":
                System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver");
                driver = new ChromeDriver();
                break;
            case "safari":
                driver = new SafariDriver();
                break;
            default:
                System.out.println("Browser name: " + driverName + " is invalid!");
        }
        if (driver != null) {
            driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(DEFAULT_TIMEOUT));
            driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(DEFAULT_TIMEOUT));
            driver.manage().timeouts().scriptTimeout(Duration.ofSeconds(DEFAULT_TIMEOUT));
            driver.manage().window().maximize();
        }
        return driver;
    }

    /**
     * Close driver
     */
    public static void closeDriver() {
        if (driver != null) {
            driver.quit();
            driver = null;
        }
    }
}
