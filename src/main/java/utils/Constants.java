package utils;

import java.util.Arrays;
import java.util.List;

/**
 * It's class with Constants
 */
public class Constants {

    public final static int DEFAULT_TIMEOUT = 30;

    public final static String PATH_TO_PROPERTY_FILE = "src/main/resources/project.properties";

    public static final List<String> categoriesList = Arrays.asList("Допомога армії", "Благодійність для українців", "Комунальні платежі",
            "Інтернет та ТБ", "Податки та збори", "Освіта та дитячі садки", "Страхові компанії", "Косметика", "Погашення кредитів / Поповнення карток",
            "Штрафи і порушення", "Туристичні компанії");

    public static final String TERMS_URL = "https://privatbank.ua/terms";
}
