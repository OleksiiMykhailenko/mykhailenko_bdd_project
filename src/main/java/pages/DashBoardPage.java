package pages;

import org.assertj.core.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static utils.Constants.TERMS_URL;

/**
 * Dash Board Page. Here are the methods that are used to work with the site dashboard
 */
public class DashBoardPage extends BasePage {

    WebDriver driver;

    public DashBoardPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public final String wNew = "//*[text()='Що нового у Приват24']";
    @FindBy(xpath = wNew)
    public WebElement whatsNew;

    public final String whatsNewPTitle = "//div[text()='Що нового у Приват24']";
    @FindBy(xpath = whatsNewPTitle)
    public WebElement whatsNewPageTitle;

    public final String payment = "//*[text()='Платіж']";
    @FindBy(xpath = payment)
    public WebElement payments;

    public final String qInput = "//input[@data-qa-node='query']";
    @FindBy(xpath = qInput)
    public WebElement queryInput;

    public final String newPayment = "//h3[text()='Новий платіж']";
    @FindBy(xpath = newPayment)
    public WebElement newPaymentTitle;

    public final String loginButtonPaymentsPage = "//button[text()='Вхід' and not(contains(@data-qa-node, 'login'))]";
    @FindBy(xpath = loginButtonPaymentsPage)
    public WebElement loginButtonOnPaymentsPage;

    public final String sMoney = "//*[text()='Переказ на картку']";
    @FindBy(xpath = sMoney)
    public WebElement sendMoney;

    public final String find = "//*[@title='Головна']//following-sibling::div[contains(@class, 'sc')]/div[1]";
    @FindBy(xpath = find)
    public WebElement findButton;

    public final String queryOfInput = "//*[@data-qa-node='query']";
    @FindBy(xpath = queryOfInput)
    public WebElement inputOfQuery;

    public final String geCard = "//button[text()='Отримати картку']";
    @FindBy(xpath = geCard)
    public WebElement getCard;

    public final String currAButton = "//button[@data-qa-node='currencyA']";
    @FindBy(xpath = currAButton)
    public WebElement currencyAButton;

    public final String currBButton = "//button[@data-qa-node='currencyB']";
    @FindBy(xpath = currBButton)
    public WebElement currencyBButton;

    public final String currencyAInput = "//input[@data-qa-node='amountA']";
    @FindBy(xpath = currencyAInput)
    public WebElement inputOfCurrencyA;

    public final String currencyBInput = "//input[@data-qa-node='amountB']";
    @FindBy(xpath = currencyBInput)
    public WebElement inputOfCurrencyB;

    public final String curRate = "//*[@data-qa-node='rate']/span[4]";
    @FindBy(xpath = curRate)
    public WebElement currencyRate;

    public final String lButton = "//button[@data-qa-node='login']";
    @FindBy(xpath = lButton)
    public WebElement loginButton;

    public final String lIFrame = "//iframe[contains(@src, 'login-widget')]";
    @FindBy(xpath = lIFrame)
    public WebElement loginIFrame;

    public final String lTitle = "//*[text()='Вхід/Реєстрація']";
    @FindBy(xpath = lTitle)
    public WebElement loginTitle;

    public final String lInput = "//input[@data-qa-node='login-number']";
    @FindBy(xpath = lInput)
    public WebElement loginInput;

    public final String tLink = "//*[contains(@href, 'terms')]";
    @FindBy(xpath = tLink)
    public WebElement termsLink;

    public final String nButton = "//button[text()='Продовжити']";
    @FindBy(xpath = nButton)
    public WebElement nextButton;

    public final String aStore = "//*[@title='App Store']";
    @FindBy(xpath = aStore)
    public WebElement appleStore;

    public final String pMarket = "//*[@title='Play Market']";
    @FindBy(xpath = pMarket)
    public WebElement playMarket;

    public static final By ELEMENTS_ON_WHATS_NEW_PAGE = By.xpath("//div[contains(@class, 'root_LSG')]");
    public static final String TEMPLATE_TITLE = "Тут будуть ваші шаблони";
    public static final String CATEGORIES_TITLE = "Категорії";
    public static final By CATEGORIES = By.cssSelector("[data-qa-node='category']");
    public static final String SEND_MONEY_MENU_TITLE = "Переказ між своїми рахунками, з/на картку VISA/MasterCard інших українських та закордонних банків.";
    public static final String FIND_QUERY_LINK = "//a[contains(text(), '%s')]";
    public static final String CURRENCY_A_BUTTON = "//button[@data-qa-node='currencyA-option' and @data-qa-value='%s']";
    public static final String CURRENCY_B_BUTTON = "//button[@data-qa-node='currencyB-option' and @data-qa-value='%s']";
    public static final String ACCEPT_WITH_TERMS = "Продовжуючи, я підтверджую, що згоден(-а) з";
    public static final String QR_CODE_IS_PRESENT = "QR-код для входу через смартфон";

    /**
     * Method that open What's New Page on Dash Board
     */
    public DashBoardPage openWhatsNewPage() {
        whatsNew.click();
        return this;
    }

    /**
     * Method that check elements on What's New Page
     */
    public DashBoardPage findElementsOnWhatsNewPageAndCheck() {
        Assertions.assertThat(whatsNewPageTitle.isEnabled()).isTrue();
        List<WebElement> contentOfPage = driver.findElements(ELEMENTS_ON_WHATS_NEW_PAGE);
        Assertions.assertThat(contentOfPage.size()).isGreaterThan(0);
        return this;
    }

    /**
     * Open My Payments menu
     */
    public DashBoardPage goToPaymentsMenu() {
        payments.click();
        assertThat(queryInput.isEnabled()).isTrue();
        assertThat(newPaymentTitle.isEnabled()).isTrue();
        return this;
    }

    /**
     * Check that templates block is present
     */
    public DashBoardPage popularTemplatesBlockPublicSessionIsPresent() {
        assertThat(loginButtonOnPaymentsPage.isEnabled()).isTrue();
        checkMessage(TEMPLATE_TITLE);
        return this;
    }

    /**
     * Check categories in category bloc
     *
     * @param expectedCategoriesValues expected categories that show's on the page
     */
    public DashBoardPage checkCategoriesList(List<String> expectedCategoriesValues) {
        checkMessage(CATEGORIES_TITLE);

        List<WebElement> categories = getWebElementsBySelector(CATEGORIES, expectedCategoriesValues.size());

        compareTextInWebElements(expectedCategoriesValues, categories);
        return this;
    }

    /**
     * Open send money menu
     */
    public DashBoardPage goToMenuSendMoneyOnPublicSessionAndCheck() {
        sendMoney.click();
        checkMessage(SEND_MONEY_MENU_TITLE);
        return this;
    }

    /**
     * Click on find button and search the result in the header of the website
     *
     * @param findQuery query string of page that we want to find
     */
    public DashBoardPage searchPage(String findQuery) {
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", findButton);
        inputOfQuery.sendKeys(findQuery);
        WebElement queryResult = driver.findElement(By.xpath(String.format(FIND_QUERY_LINK, findQuery)));
        executor.executeScript("arguments[0].click();", queryResult);
        return this;
    }

    /**
     * Check elements are present on Instant Installment Page
     *
     * @param title title of the page
     */
    public DashBoardPage checkElementsOnInstantInstallmentPage(String title) {
        org.junit.jupiter.api.Assertions.assertEquals(driver.findElement(By.xpath("//h1")).getText(), title);
        assertThat(getCard.isDisplayed()).isTrue();
        return this;
    }

    /**
     * Choose currency in Currency Converter
     *
     * @param currencyA Set first currency, variants - EUR/USD/GBP/PLN/UAH
     * @param currencyB Set second currency, variants - EUR/USD/GBP/PLN/UAH
     */
    public DashBoardPage chooseCurrency(String currencyA, String currencyB) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", currencyAButton);
        currencyAButton.click();
        driver.findElement(By.xpath(String.format(CURRENCY_A_BUTTON, currencyA))).click();
        assertEquals(currencyAButton.getAttribute("data-qa-value"), currencyA);

        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", currencyBButton);
        currencyBButton.click();
        driver.findElement(By.xpath(String.format(CURRENCY_B_BUTTON, currencyB))).click();
        assertEquals(currencyBButton.getAttribute("data-qa-value"), currencyB);
        return this;
    }

    /**
     * Set the value and check the value in field B according to the current exchange rate
     *
     * @param value value that we want to exchange
     */
    public DashBoardPage setCurrencyValueAndCheckRate(String value) {
        inputOfCurrencyA.click();
        inputOfCurrencyA.sendKeys(value);

        String rate = currencyRate.getText();
        Double converterValue = Double.parseDouble(value) * Double.parseDouble(rate);
        converterValue = Math.round(converterValue * 100.0) / 100.0;
        String stringConverterValue = String.valueOf(converterValue);

        assertEquals(inputOfCurrencyB.getAttribute("data-qa-value"), stringConverterValue);
        return this;
    }

    /**
     * Move to Login button, click and check that login frame is present
     */
    public DashBoardPage moveToLoginFrame() {
        assertThat(loginButton.isDisplayed()).isTrue();
        Actions action = new Actions(driver);
        action.moveToElement(loginButton).click().perform();
        return this;
    }

    /**
     * Check All elements on Login frame
     */
    public DashBoardPage checkElementOnLoginFrame() {
        driver.switchTo().frame(loginIFrame);

        assertThat(loginTitle.isDisplayed()).isTrue();
        assertThat(loginInput.isDisplayed()).isTrue();
        assertThat(termsLink.isDisplayed()).isTrue();

        checkMessage(ACCEPT_WITH_TERMS);

        termsLink.click();
        String winHandleBefore = driver.getWindowHandle();
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }

        String currentUrl = driver.getCurrentUrl();
        assertThat(currentUrl).contains(TERMS_URL);

        driver.close();
        driver.switchTo().window(winHandleBefore);

        driver.switchTo().frame(loginIFrame);

        assertThat(nextButton.isDisplayed()).isTrue();

        checkMessage(QR_CODE_IS_PRESENT);

        assertThat(appleStore.isDisplayed()).isTrue();
        assertThat(playMarket.isDisplayed()).isTrue();

        driver.switchTo().defaultContent();
        return this;
    }
}
