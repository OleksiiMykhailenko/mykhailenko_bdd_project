package pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;
import static utils.Constants.DEFAULT_TIMEOUT;

/**
 * Base page. Here are the methods that will be used throughout the project
 */
public class BasePage {

    WebDriver driver;

    public final String inputOfDebit = "//input[@data-qa-node='numberdebitSource']";
    @FindBy(xpath = inputOfDebit)
    private WebElement inputOfDebitCard;

    public final String inputOfExpire = "//input[@data-qa-node='expiredebitSource']";
    @FindBy(xpath = inputOfExpire)
    public WebElement inputOfExpireDate;

    public final String inputCVV = "//input[@data-qa-node='cvvdebitSource']";
    @FindBy(xpath = inputCVV)
    public WebElement inputOfCVV;

    public final String inputOfReceiver = "//input[@data-qa-node='numberreceiver']";
    @FindBy(xpath = inputOfReceiver)
    public WebElement inputOfReceiverCard;

    public final String inputAmount = "//input[@data-qa-node='amount']";
    @FindBy(xpath = inputAmount)
    public WebElement inputOfAmount;

    public final String cButton = "//button[@data-qa-node='currency']";
    @FindBy(xpath = cButton)
    public WebElement currencyButton;

    public final String commButton = "//*[@data-qa-node='toggle-comment']";
    @FindBy(xpath = commButton)
    public WebElement commentButton;

    public final String inputComment = "//*[@name='comment']";
    @FindBy(xpath = inputComment)
    public WebElement inputOfComment;

    public final String sButton = "//button[text()='Переказати']";
    @FindBy(xpath = sButton)
    public WebElement sendButton;

    public static final String TEXT = "//*[text()='%s']";
    public static final String LIST_NOT_EQUAL_MESSAGE = "Actual list - {%s} \n does not equal expected - {%s}";
    public static final String CHOOSE_CURRENCY = "//button[@data-qa-node='currency-option' and @data-qa-value='%s']";

    public BasePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    /**
     * Checking whether the message text is on the page
     *
     * @param message the message we are looking for
     */
    public void checkMessage(String message) {
        assertThat(driver.findElement(By.xpath(String.format(TEXT, message))).isEnabled()).isTrue();
    }

    /**
     * Check that our List WebElement by selector has size that we expected
     *
     * @param selector     selector web element
     * @param expectedSize size that we expected
     */
    public List<WebElement> getWebElementsBySelector(By selector, int expectedSize) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(DEFAULT_TIMEOUT));
        wait.until(ExpectedConditions.numberOfElementsToBe(selector, expectedSize));
        return driver.findElements(selector);
    }

    /**
     * Compare List of elements
     *
     * @param expectedTextList  expected list of elements
     * @param actualWebElements actual list of elements
     */
    public static void compareTextInWebElements(List<String> expectedTextList, List<WebElement> actualWebElements) {
        List<String> actualTextList = actualWebElements.stream().map(WebElement::getText).toList();
        boolean isEqual = IntStream.range(0, Math.min(expectedTextList.size(), actualWebElements.size()))
                .parallel()
                .allMatch(i -> expectedTextList.get(i).equals(actualTextList.get(i)));
        Assert.assertTrue(LIST_NOT_EQUAL_MESSAGE, isEqual);
    }

    /**
     * Filling in all fields of the sender's card
     *
     * @param numberOfCard sender's card number
     * @param expiredDate  sender's card expiry date
     * @param cvv          sender's card cvv
     */
    public BasePage fillFieldsSendersCard(String numberOfCard, String expiredDate, String cvv) {
        inputOfDebitCard.sendKeys(numberOfCard);
        inputOfExpireDate.sendKeys(expiredDate);
        inputOfCVV.sendKeys(cvv);
        return this;
    }

    /**
     * Filling in all fields of the receiver card
     *
     * @param numberOfCard receiver card number
     */
    public BasePage fillFieldsReceiverCard(String numberOfCard) {
        inputOfReceiverCard.sendKeys(numberOfCard);
        return this;
    }

    /**
     * Filling out the amount field
     *
     * @param amount amount
     */
    public BasePage fillAmount(String amount) {
        inputOfAmount.sendKeys(amount);
        return this;
    }

    /**
     * Choose the currency
     *
     * @param currency variant of the currency. There are variants - UAH/USD/EUR
     */
    public BasePage chooseCurrency(String currency) {
        currencyButton.click();
        driver.findElement(By.xpath(String.format(CHOOSE_CURRENCY, currency))).click();
        return this;
    }

    /**
     * Add comment
     *
     * @param comment message that you write
     */
    public BasePage addComment(String comment) {
        commentButton.click();
        inputOfComment.sendKeys(comment);
        return this;
    }

    /**
     * Click Send Button
     */
    public BasePage clickSendButton() {
        sendButton.click();
        return this;
    }
}
